package org.example.valueObjects;

public class Money {
    private double ammount;
    private String currency;
    private String mark;

    public Money(double ammount, String currency, String mark) {
        this.ammount = ammount;
        this.currency = currency;
        this.mark = mark;
    }

    public double getAmmount() {
        return ammount;
    }

    public void setAmmount(double ammount) {
        this.ammount = ammount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
