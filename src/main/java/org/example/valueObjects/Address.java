package org.example.valueObjects;

public class Address {

    private String street;
    private Long number;
    private Long postcode;
    private String ciy;

    public Address(String street, Long number, Long postcode, String ciy) {
        this.street = street;
        this.number = number;
        this.postcode = postcode;
        this.ciy = ciy;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Long getPostcode() {
        return postcode;
    }

    public void setPostcode(Long postcode) {
        this.postcode = postcode;
    }

    public String getCiy() {
        return ciy;
    }

    public void setCiy(String ciy) {
        this.ciy = ciy;
    }



}
